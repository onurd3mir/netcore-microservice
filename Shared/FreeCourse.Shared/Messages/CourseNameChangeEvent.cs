﻿namespace FreeCourse.Shared.Messages
{
    public class CourseNameChangeEvent
    {
        public string CourseId { get; set; }
        public string CourseName { get; set; }
    }
}
