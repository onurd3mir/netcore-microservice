﻿using FreeCourse.Shared.Dtos;
using FreeCourse.Shared.Services;
using FreeCourse.WebMVC.Models.Orders;
using FreeCourse.WebMVC.Models.Payments;
using FreeCourse.WebMVC.Services.Interfaces;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services
{
    public class OrderService : IOrderService
    {
        private readonly HttpClient _httpClient;
        private readonly IDiscountService _discountService;
        private readonly IBasketService _basketService;
        private readonly IPaymentService _paymentService;
        private readonly ISharedIdentityService _sharedIdentityService;

        public OrderService(HttpClient httpClient, IDiscountService discountService, IBasketService basketService, IPaymentService paymentService,ISharedIdentityService sharedIdentityService)
        {
            _httpClient = httpClient;
            _discountService = discountService;
            _basketService = basketService;
            _paymentService = paymentService;
            _sharedIdentityService= sharedIdentityService;
        }

        public async Task<OrderCreatedViewModel> CreateOrder(CheckoutInfoInput checkoutInfoInput)
        {
            var basket = await _basketService.Get();

            var paymentInfoInput = new PaymentInfoInput
            {
                CardName = checkoutInfoInput.CardName,
                CardNumber = checkoutInfoInput.CardNumber,
                Expiration = checkoutInfoInput.Expiration,
                Cvv = checkoutInfoInput.Cvv,
                TotalPrice = basket.TotalPrice,
            };

            var paymentRes = await _paymentService.ReceivePayment(paymentInfoInput);

            if (!paymentRes)
            {
                return new OrderCreatedViewModel { Error = "Ödeme Alınamadı", IsSuccessFull = false };
            }

            var orderCreateInput = new OrderCreateInput
            {
                Address = new AddressInput
                {
                    Province = checkoutInfoInput.Province,
                    District = checkoutInfoInput.District,
                    Street = checkoutInfoInput.Street,
                    Line = checkoutInfoInput.Line,
                    ZipCode = checkoutInfoInput.ZipCode
                }
            };

            basket.BasketItems.ForEach(x =>
            {
                var orderItem = new OrderItemCreateInput
                {
                    ProductId = x.CourseId,
                    Price = x.GetCurrentPrice,
                    PictureUrl = "",
                    ProductName = x.CourseName
                };
                orderCreateInput.OrderItems.Add(orderItem);
            });

            var response = await _httpClient.PostAsJsonAsync<OrderCreateInput>("order/saveorder", orderCreateInput);

            if (!response.IsSuccessStatusCode)
            {
                return new OrderCreatedViewModel { Error = "Sipariş Oluşturulamadı", IsSuccessFull = false };
            }

            var order = await response.Content.ReadFromJsonAsync<Response<OrderCreatedViewModel>>();
            order.Data.IsSuccessFull = true;
            await _basketService.Delete();

            return order.Data;
        }

        public async Task<List<OrderViewModel>> GetOrder()
        {
            var response = await _httpClient.GetFromJsonAsync<Response<List<OrderViewModel>>>("order/getorders");
            return response.Data;
        }

        public async Task<OrderSuspendViewModel> SuspendOrder(CheckoutInfoInput checkoutInfoInput)
        {
            var basket = await _basketService.Get();

            var orderCreateInput = new OrderCreateInput
            {
                BuyerId = _sharedIdentityService.GetUserId,
                Address = new AddressInput
                {
                    Province = checkoutInfoInput.Province,
                    District = checkoutInfoInput.District,
                    Street = checkoutInfoInput.Street,
                    Line = checkoutInfoInput.Line,
                    ZipCode = checkoutInfoInput.ZipCode
                }
            };

            basket.BasketItems.ForEach(x =>
            {
                var orderItem = new OrderItemCreateInput
                {
                    ProductId = x.CourseId,
                    Price = x.GetCurrentPrice,
                    PictureUrl = "",
                    ProductName = x.CourseName
                };
                orderCreateInput.OrderItems.Add(orderItem);
            });

            var paymentInfoInput = new PaymentInfoInput
            {
                CardName = checkoutInfoInput.CardName,
                CardNumber = checkoutInfoInput.CardNumber,
                Expiration = checkoutInfoInput.Expiration,
                Cvv = checkoutInfoInput.Cvv,
                TotalPrice = basket.TotalPrice,
                Order = orderCreateInput,
            };

            var paymentRes = await _paymentService.ReceivePayment(paymentInfoInput);

            if (!paymentRes)
            {
                return new OrderSuspendViewModel { Error = "Ödeme Alınamadı", IsSuccessFull = false };
            }

            await _basketService.Delete();
            return new OrderSuspendViewModel { IsSuccessFull = true };
        }
    }
}
