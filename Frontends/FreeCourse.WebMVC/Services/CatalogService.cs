﻿using FreeCourse.Shared.Dtos;
using FreeCourse.WebMVC.Models.Catalogs;
using FreeCourse.WebMVC.Services.Interfaces;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly HttpClient _httpClient;
        private readonly IPhotoStockService _photoStockService;
       
        public CatalogService(HttpClient httpClient, IPhotoStockService photoStockService)
        {
            _httpClient = httpClient;
            _photoStockService = photoStockService;
        }

        public async Task<bool> CreateCourseAsync(CourseCreateModel courseCreateModel)
        {
            var resultPhoto = await _photoStockService.UploadPhoto(courseCreateModel.PhotoFormFile);

            if (resultPhoto!=null)
            {
                courseCreateModel.Picture = resultPhoto.Url;
            }

            var response = await _httpClient.PostAsJsonAsync("course/create", courseCreateModel);
            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteCourseAsync(string courseId)
        {
            var course = await GetByCourseIdAsync(courseId);

            if (course!=null)
            {
                await _photoStockService.DeletePhoto(course.Picture);
            }

            var response = await _httpClient.DeleteAsync("course/delete/" + courseId);
            return response.IsSuccessStatusCode;
        }

        public async Task<List<CategoryViewModel>> GetAllCategoryAsync()
        {
            var response = await _httpClient.GetAsync("category/getall");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
            var responseSuccess = await response.Content.ReadFromJsonAsync<Response<List<CategoryViewModel>>>();
            return responseSuccess.Data;
        }

        public async Task<List<CourseViewModel>> GetAllCourseAsync()
        {
            var response = await _httpClient.GetAsync("course/getall");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
            var responseSuccess = await response.Content.ReadFromJsonAsync<Response<List<CourseViewModel>>>();
            return responseSuccess.Data;
        }

        public async Task<List<CourseViewModel>> GetAllCourseByUserIdAsync(string userId)
        {
            var response = await _httpClient.GetAsync("course/getallbyuserid?userId=" + userId);
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
            var responseSuccess = await response.Content.ReadFromJsonAsync<Response<List<CourseViewModel>>>();
            return responseSuccess.Data;
        }

        public async Task<CourseViewModel> GetByCourseIdAsync(string courseId)
        {
            var response = await _httpClient.GetAsync("course/getbyid?id=" + courseId);
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
            var responseSuccess = await response.Content.ReadFromJsonAsync<Response<CourseViewModel>>();
            return responseSuccess.Data;
        }

        public async Task<bool> UpdateCourseAsync(CourseUpdateModel courseUpdateModel)
        {
            if (courseUpdateModel.PhotoFormFile!=null && courseUpdateModel.PhotoFormFile.Length>0)
            {
                await _photoStockService.DeletePhoto(courseUpdateModel.Picture);
                var resultPhoto = await _photoStockService.UploadPhoto(courseUpdateModel.PhotoFormFile);

                if (resultPhoto != null)
                {
                    courseUpdateModel.Picture = resultPhoto.Url;
                }
            }
            var response = await _httpClient.PutAsJsonAsync("course/update", courseUpdateModel);
            return response.IsSuccessStatusCode;
        }
    }
}
