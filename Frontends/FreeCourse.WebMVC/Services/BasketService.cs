﻿using FreeCourse.Shared.Dtos;
using FreeCourse.WebMVC.Models.Baskets;
using FreeCourse.WebMVC.Services.Interfaces;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services
{
    public class BasketService : IBasketService
    {
        private readonly HttpClient _httpClient;
        private readonly IDiscountService _discountService;
        public BasketService(HttpClient httpClient, IDiscountService discountService)
        {
            _httpClient = httpClient;
            _discountService = discountService;
        }

        public async Task AddBasketItem(BasketItemViewModel model)
        {
            var basket = await Get();

            if (basket != null)
            {
                if (!basket.BasketItems.Any(x => x.CourseId == model.CourseId))
                {
                    basket.BasketItems.Add(model);
                }
            }
            else
            {
                basket = new BasketViewModel();
                basket.BasketItems.Add(model);
            }

            await SaveOrUpdate(basket);
        }

        public async Task<bool> ApplyDiscount(string discountCode)
        {
            await CancelApplyDiscount();
            var basket = await Get();

            if (basket == null && discountCode == null)
            {
                return false;
            }

            var getDiscount = await _discountService.GetDiscount(discountCode);

            if (getDiscount == null)
            {
                return false;
            }

            basket.ApplyDiscount(getDiscount.Code, getDiscount.Rate);
            await SaveOrUpdate(basket);
            return true;
        }

        public async Task<bool> CancelApplyDiscount()
        {
            var basket = await Get();
            if (basket == null && basket.DiscountCode == null)
            {
                return false;
            }
            basket.CancelDiscount();
            await SaveOrUpdate(basket);
            return true;
        }

        public async Task<bool> Delete()
        {
            var result = await _httpClient.DeleteAsync("basket/deletebasket");
            return result.IsSuccessStatusCode;
        }

        public async Task<BasketViewModel> Get()
        {
            var response = await _httpClient.GetAsync("basket/getbasket");

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var model = await response.Content.ReadFromJsonAsync<Response<BasketViewModel>>();
            return model.Data;
        }

        public async Task<bool> RemoveBaskteItem(string courseId)
        {
            var basket = await Get();

            if (basket == null) return false;

            var deleteBasketItem = basket.BasketItems.FirstOrDefault(x => x.CourseId == courseId);

            if (deleteBasketItem == null) return false;

            var deleteResult = basket.BasketItems.Remove(deleteBasketItem);

            if (!deleteResult) return false;

            if (!basket.BasketItems.Any()) basket.DiscountCode = null;

            return await SaveOrUpdate(basket);
        }

        public async Task<bool> SaveOrUpdate(BasketViewModel model)
        {
            var response = await _httpClient.PostAsJsonAsync<BasketViewModel>("basket/saveorupdatebasket", model);
            return response.IsSuccessStatusCode;
        }
    }
}
