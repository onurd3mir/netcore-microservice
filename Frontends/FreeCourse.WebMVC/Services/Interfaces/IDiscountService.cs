﻿using FreeCourse.WebMVC.Models.Discounts;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services.Interfaces
{
    public interface IDiscountService
    {
        Task<DiscountViewModel> GetDiscount(string code);
    }
}
