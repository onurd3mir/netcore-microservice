﻿using FreeCourse.Shared.Dtos;
using FreeCourse.WebMVC.Models;
using IdentityModel.Client;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services.Interfaces
{
    public interface IIdentityService
    {
        Task<Response<bool>> SignIn(SigInInput sigInInput);
        Task<TokenResponse> GetAccessTokenByRefreshToken();
        Task RevokeRefreshToken();
    }
}
