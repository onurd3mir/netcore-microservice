﻿using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services.Interfaces
{
    public interface IClientCredantialTokenService
    {
        Task<string> GetToken();
    }
}
