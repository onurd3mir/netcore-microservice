﻿using FreeCourse.WebMVC.Models.Catalogs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services.Interfaces
{
    public interface ICatalogService
    {
        Task<List<CourseViewModel>> GetAllCourseAsync();
        Task<List<CourseViewModel>> GetAllCourseByUserIdAsync(string userId);
        Task<CourseViewModel> GetByCourseIdAsync(string courseId);
        Task<List<CategoryViewModel>> GetAllCategoryAsync();
        Task<bool> CreateCourseAsync(CourseCreateModel courseCreateModel);
        Task<bool> UpdateCourseAsync(CourseUpdateModel courseUpdateModel);
        Task<bool> DeleteCourseAsync(string courseId);
    }
}
