﻿using FreeCourse.WebMVC.Models.Payments;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<bool> ReceivePayment(PaymentInfoInput paymentInfoInput);
    }
}
