﻿using FreeCourse.Shared.Services;
using FreeCourse.WebMVC.Models.Catalogs;
using FreeCourse.WebMVC.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Controllers
{
    [Authorize]
    public class CourseController : Controller
    {
        private readonly ICatalogService _catalogService;
        private readonly ISharedIdentityService _sharedIdentityService;

        public CourseController(ICatalogService catalogService, ISharedIdentityService sharedIdentityService)
        {
            _catalogService = catalogService;
            _sharedIdentityService = sharedIdentityService;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _catalogService.GetAllCourseByUserIdAsync(_sharedIdentityService.GetUserId));
        }

        public async Task<IActionResult> Create()
        {
            var categories = await _catalogService.GetAllCategoryAsync();
            ViewBag.categoryList = new SelectList(categories, "Id", "Name");
            return View();
        }

        [HttpPost]  
        [ValidateAntiForgeryToken]  
        public async Task<IActionResult> Create(CourseCreateModel courseCreateModel)
        {
            var categories = await _catalogService.GetAllCategoryAsync();
            ViewBag.categoryList = new SelectList(categories, "Id", "Name");
            if (!ModelState.IsValid)
            {
                return View();
            }
            courseCreateModel.UserId = _sharedIdentityService.GetUserId;
            await _catalogService.CreateCourseAsync(courseCreateModel); 
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(string id)
        {
            var course = await _catalogService.GetByCourseIdAsync(id);
          
            if (course==null)
            {
                ViewBag.viewError = "Kurs Bulunamadı";
                return RedirectToAction("Index");
            }

            var categories = await _catalogService.GetAllCategoryAsync();
            ViewBag.categoryList = new SelectList(categories, "Id", "Name",course.CategoryId);

            CourseUpdateModel courseUpdateModel = new()
            {
                Id = course.Id,
                Name = course.Name,
                Description = course.Description,
                Price=course.Price,
                Feature=course.Feature,
                CategoryId=course.CategoryId,
                UserId=course.UserId,
                Picture = course.Picture,
            };

            return View(courseUpdateModel);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CourseUpdateModel courseUpdateModel)
        {
            if (!ModelState.IsValid)
            {
                var categories = await _catalogService.GetAllCategoryAsync();
                ViewBag.categoryList = new SelectList(categories, "Id", "Name", courseUpdateModel.CategoryId);
                return View(courseUpdateModel);
            }
            await _catalogService.UpdateCourseAsync(courseUpdateModel);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Delete(string id)
        {
            var course = await _catalogService.GetByCourseIdAsync(id);
            if (course == null)
            {
                ViewBag.viewError = "Kurs Bulunamadı";
                return RedirectToAction("Index");
            }
            await _catalogService.DeleteCourseAsync(id);
            return RedirectToAction("Index");
        }
    }
}
