﻿using FreeCourse.WebMVC.Models.Orders;
using FreeCourse.WebMVC.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Controllers
{
    public class OrderController : Controller
    {
        private readonly IBasketService _basketService;
        private readonly IOrderService _orderService;

        public OrderController(IBasketService basketService, IOrderService orderService)
        {
            _basketService = basketService;
            _orderService = orderService;
        }

        public async Task<IActionResult> Checkout()
        {
            var basket = await _basketService.Get();
            ViewBag.basket = basket;
            return View(new CheckoutInfoInput());
        }

        [HttpPost]
        public async Task<IActionResult> Checkout(CheckoutInfoInput checkoutInfoInput)
        {
            //1.yol senkron iletişim
            //var orderStatus = await _orderService.CreateOrder(checkoutInfoInput);
            var orderSuspend = await _orderService.SuspendOrder(checkoutInfoInput);

            if (!orderSuspend.IsSuccessFull)
            {
                var basket = await _basketService.Get();
                ViewBag.basket = basket;
                ViewBag.error = orderSuspend.Error;
                return View();
            }
            //1. yol
            //return RedirectToAction("SuccessFullCheckout", new { orderId = orderStatus.OrderId });
            return RedirectToAction("SuccessFullCheckout", new { orderId = new Random().Next(10000,99999) });
        }

        public IActionResult SuccessFullCheckout(int orderId)
        {
            ViewBag.orderId = orderId;
            return View();
        }

        public async Task<IActionResult> CheckoutHistorty()
        {
            return View(await _orderService.GetOrder());
        }
    }
}
