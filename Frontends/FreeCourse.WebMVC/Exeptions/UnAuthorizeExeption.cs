﻿using System;
using System.Runtime.Serialization;

namespace FreeCourse.WebMVC.Exeptions
{
    public class UnAuthorizeExeption : Exception
    {
        public UnAuthorizeExeption()
        {
        }

        public UnAuthorizeExeption(string message) : base(message)
        {
        }

        public UnAuthorizeExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnAuthorizeExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
