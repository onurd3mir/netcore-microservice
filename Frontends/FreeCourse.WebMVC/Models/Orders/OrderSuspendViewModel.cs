﻿namespace FreeCourse.WebMVC.Models.Orders
{
    public class OrderSuspendViewModel
    {
        public string Error { get; set; }

        public bool IsSuccessFull { get; set; }
    }
}
