﻿namespace FreeCourse.WebMVC.Models.Baskets
{
    public class BasketItemViewModel
    {
        public int Quantity { get; set; }
        public string CourseId { get; set; }
        public string CourseName { get; set; }
        public decimal Price { get; set; }
        private decimal? DiscountAppliedPrice;
        public decimal GetCurrentPrice
        {
            get => DiscountAppliedPrice ?? Price;
        }
        public void AppliedDiscount(decimal dicoundtPrice)
        {
            DiscountAppliedPrice = dicoundtPrice;
        }
    }
}
