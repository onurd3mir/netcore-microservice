﻿using System;

namespace FreeCourse.WebMVC.Models.Discounts
{
    public class DiscountViewModel
    {
        public string UserId { get; set; }
        public int Rate { get; set; }
        public string Code { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
