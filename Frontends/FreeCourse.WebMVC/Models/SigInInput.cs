﻿using System.ComponentModel.DataAnnotations;

namespace FreeCourse.WebMVC.Models
{
    public class SigInInput
    {
        [Required]
        [Display(Name ="Email Adresiniz")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Şifresniz")]
        public string Password { get; set; }

        [Display(Name = "Beni Hatırla")]
        public bool IsRemember { get; set; }
    }
}
