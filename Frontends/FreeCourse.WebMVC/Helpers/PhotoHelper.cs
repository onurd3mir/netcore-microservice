﻿using FreeCourse.WebMVC.Models;
using Microsoft.Extensions.Options;

namespace FreeCourse.WebMVC.Helpers
{
    public class PhotoHelper
    {
        private static string photoStockUrl = "http://localhost:5012";

        public static string GetPhotoStockUrl(string photo)
        {
            return $"{photoStockUrl}/photos/{photo}";
        }

    }
}
