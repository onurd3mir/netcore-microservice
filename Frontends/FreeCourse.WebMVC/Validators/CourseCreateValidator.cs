﻿using FluentValidation;
using FreeCourse.WebMVC.Models.Catalogs;

namespace FreeCourse.WebMVC.Validators
{
    public class CourseCreateValidator:AbstractValidator<CourseCreateModel>
    {
        public CourseCreateValidator()
        {
            RuleFor(x=>x.Name).NotEmpty().WithMessage("Kurs ismi Boş Olamaz");
            RuleFor(x => x.Description).NotEmpty().WithMessage("Kurs Açıklama Alanı Boş Olmaz");
            RuleFor(x => x.Feature.Duration).InclusiveBetween(1, int.MaxValue).WithMessage("Süre Alanı Boş Olamaz");
            RuleFor(x => x.Price).NotEmpty().WithMessage("Fiyat Alanı Boş Olamaz").ScalePrecision(2, 6).WithMessage("Hatalı Format");
            RuleFor(x => x.CategoryId).NotEmpty().WithMessage("Kategori Alanı Seçiniz");
        }
    }
}
