﻿using FluentValidation;
using FreeCourse.WebMVC.Models.Discounts;

namespace FreeCourse.WebMVC.Validators
{
    public class DiscountInputValidator: AbstractValidator<DiscountApplyInput>
    {
        public DiscountInputValidator()
        {
            RuleFor(x=>x.Code).NotEmpty().WithMessage("İndirim Kodu Boş Olamaz!");
        }
    }
}
