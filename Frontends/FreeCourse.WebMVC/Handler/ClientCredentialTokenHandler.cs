﻿using FreeCourse.WebMVC.Exeptions;
using FreeCourse.WebMVC.Services.Interfaces;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace FreeCourse.WebMVC.Handler
{
    public class ClientCredentialTokenHandler : DelegatingHandler
    {
        private readonly IClientCredantialTokenService _clientCredantialTokenService;

        public ClientCredentialTokenHandler(IClientCredantialTokenService clientCredantialTokenService)
        {
            _clientCredantialTokenService = clientCredantialTokenService;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", await _clientCredantialTokenService.GetToken());

            var response = await base.SendAsync(request, cancellationToken);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new UnAuthorizeExeption();
            }

            return response;
        }
    }
}
