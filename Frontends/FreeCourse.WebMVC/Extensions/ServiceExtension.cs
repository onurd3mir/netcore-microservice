﻿using FreeCourse.WebMVC.Handler;
using FreeCourse.WebMVC.Models;
using FreeCourse.WebMVC.Services;
using FreeCourse.WebMVC.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace FreeCourse.WebMVC.Extensions
{
    public static class ServiceExtension
    {
        public static void AddHttpClientServices(this IServiceCollection services, IConfiguration Configuration)
        {
            var serviceApiSettigns = Configuration.GetSection("ServiceApiSettings").Get<ServiceApiSettings>();

            services.AddHttpClient<IClientCredantialTokenService, ClientCredentialTokenService>();
            services.AddHttpClient<IIdentityService, IdentityService>();

            services.AddHttpClient<ICatalogService, CatalogService>(opt =>
            {
                opt.BaseAddress = new Uri($"{serviceApiSettigns.GatewayBaseUri}/{serviceApiSettigns.Catalog.Endpoint}");
            }).AddHttpMessageHandler<ClientCredentialTokenHandler>();

            services.AddHttpClient<IPhotoStockService, PhotoStockService>(opt =>
            {
                opt.BaseAddress = new Uri($"{serviceApiSettigns.GatewayBaseUri}/{serviceApiSettigns.PhotoStock.Endpoint}");
            }).AddHttpMessageHandler<ClientCredentialTokenHandler>();

            services.AddHttpClient<IUserService, UserService>(opt =>
            {
                opt.BaseAddress = new Uri(serviceApiSettigns.IdentityBaseUri);
            }).AddHttpMessageHandler<RecourseOwnerPasswordTokenHandler>();

            services.AddHttpClient<IBasketService, BasketService>(opt =>
            {
                opt.BaseAddress = new Uri($"{serviceApiSettigns.GatewayBaseUri}/{serviceApiSettigns.Basket.Endpoint}");
            }).AddHttpMessageHandler<RecourseOwnerPasswordTokenHandler>();

            services.AddHttpClient<IDiscountService, DiscountService>(opt =>
            {
                opt.BaseAddress = new Uri($"{serviceApiSettigns.GatewayBaseUri}/{serviceApiSettigns.Discount.Endpoint}");
            }).AddHttpMessageHandler<RecourseOwnerPasswordTokenHandler>();

            services.AddHttpClient<IPaymentService, PaymentService>(opt =>
            {
                opt.BaseAddress = new Uri($"{serviceApiSettigns.GatewayBaseUri}/{serviceApiSettigns.Payment.Endpoint}");
            }).AddHttpMessageHandler<RecourseOwnerPasswordTokenHandler>();

            services.AddHttpClient<IOrderService, OrderService>(opt =>
            {
                opt.BaseAddress = new Uri($"{serviceApiSettigns.GatewayBaseUri}/{serviceApiSettigns.Order.Endpoint}");
            }).AddHttpMessageHandler<RecourseOwnerPasswordTokenHandler>();
        }
    }
}
