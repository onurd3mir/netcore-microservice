﻿using FreeCourse.Services.PhotoStock.Dtos;
using FreeCourse.Shared.ControllerBases;
using FreeCourse.Shared.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace FreeCourse.Services.PhotoStock.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : CustomControllerBase
    {
        [HttpPost("photosave")]
        public async Task<IActionResult> PhotoSave(IFormFile photo,CancellationToken cancellationToken)
        {
            if (photo!=null && photo.Length>0)
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/photos", photo.FileName);
                using var stream = new FileStream(path, FileMode.Create);
                await photo.CopyToAsync(stream, cancellationToken);
                var returnPath = photo.FileName;
                PhotoDto photoDto = new() { Url = returnPath };
                return CreateActionResultIntanse(Response<PhotoDto>.Success(photoDto, 200));
            }
            return CreateActionResultIntanse(Response<NoContent>.Fail("photo not emtry", 400));
        }

        [HttpGet("photodelete")]
        public IActionResult PhotoDelete(string photName)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/photos", photName);
            if (!System.IO.File.Exists(path))
            {
                return CreateActionResultIntanse(Response<NoContent>.Fail("photo not found", 400));
            }
            System.IO.File.Delete(path);
            return CreateActionResultIntanse(Response<NoContent>.Success(200));
        }
    }
}
