﻿using FreeCourse.Services.Basket.Services;
using FreeCourse.Shared.Messages;
using MassTransit;
using System.Linq;
using System.Threading.Tasks;

namespace FreeCourse.Services.Basket.Consumer
{
    public class CourseNameChangeEventConsumer : IConsumer<CourseNameChangeEvent>
    {
        private readonly RedisService _redisService;
        private readonly IBasketService _basketService;

        public CourseNameChangeEventConsumer(RedisService redisService, IBasketService basketService)
        {
            _redisService = redisService;
            _basketService = basketService;
        }

        public async Task Consume(ConsumeContext<CourseNameChangeEvent> context)
        {
            var keys = _redisService.GetKeys();
            if (keys != null)
            {
                foreach (var key in keys)
                {
                    var basket = await _basketService.GetBasket(key);
                    var basketItem = basket.Data.BasketItems.Where(x => x.CourseId == context.Message.CourseId).ToList();
                    basketItem.ForEach(x =>
                    {
                        x.CourseName = context.Message.CourseName;
                    });
                    await _basketService.SaveOrUpdate(basket.Data);
                }
            }
        }
    }
}
