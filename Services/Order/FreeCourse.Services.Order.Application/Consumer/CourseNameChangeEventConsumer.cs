﻿using FreeCourse.Services.Order.Infrastructure;
using FreeCourse.Shared.Messages;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace FreeCourse.Services.Order.Application.Consumer
{
    public class CourseNameChangeEventConsumer : IConsumer<CourseNameChangeEvent>
    {
        private readonly OrderDbContext _orderContext;
        public CourseNameChangeEventConsumer(OrderDbContext orderContext)
        {
            _orderContext = orderContext;
        }
        public async Task Consume(ConsumeContext<CourseNameChangeEvent> context)
        {
            var orderItems = await _orderContext.OrderItems.Where(x => x.ProductId == context.Message.CourseId).ToListAsync();

            if (orderItems != null)
            {
                orderItems.ForEach(x =>
                {
                    x.UpdateOrderItem(context.Message.CourseName, x.PictureUrl, x.Price);
                });

                await _orderContext.SaveChangesAsync();
            }
        }
    }
}
