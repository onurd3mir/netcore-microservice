﻿using FreeCourse.Services.Order.Infrastructure;
using FreeCourse.Shared.Messages;
using MassTransit;
using System.Threading.Tasks;

namespace FreeCourse.Services.Order.Application.Consumer
{
    public class CreateOrderMessageCommadConsumer : IConsumer<CreateOrderMessageCommand>
    {
        private readonly OrderDbContext _orderContext;

        public CreateOrderMessageCommadConsumer(OrderDbContext orderContext)
        {
            _orderContext = orderContext;
        }
        public async Task Consume(ConsumeContext<CreateOrderMessageCommand> context)
        {
            var newAddress = new Domain.OrderAggregateRoot.Address(
                context.Message.Province,
                context.Message.District,
                context.Message.Street,
                context.Message.ZipCode,
                context.Message.Line
                );

            var order = new Domain.OrderAggregateRoot.Order(context.Message.BuyerId,newAddress);
            context.Message.OrderItems.ForEach(x =>
            {
                order.AddOrderItem(x.ProductId, x.ProductName, x.Price, x.PictureUrl);
            });

            await _orderContext.Orders.AddAsync(order);
            await _orderContext.SaveChangesAsync();
        }
    }
}
