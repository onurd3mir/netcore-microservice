﻿using AutoMapper;
using FreeCourse.Services.Order.Application.Dtos;

namespace FreeCourse.Services.Order.Application.Mapping
{
    public class CustomMapping : Profile
    {
        public CustomMapping()
        {
            CreateMap<Domain.OrderAggregateRoot.Order, OrderDto>().ReverseMap();
            CreateMap<Domain.OrderAggregateRoot.OrderItem, OrderItemDto>().ReverseMap();
            CreateMap<Domain.OrderAggregateRoot.Address, AddressDto>().ReverseMap();
        }
    }
}
