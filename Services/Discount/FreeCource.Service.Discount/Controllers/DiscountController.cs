﻿using FreeCource.Service.Discount.Services;
using FreeCourse.Shared.ControllerBases;
using FreeCourse.Shared.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FreeCource.Service.Discount.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiscountController : CustomControllerBase
    {
        private readonly IDiscountService _discountService;
        private readonly ISharedIdentityService _sharedIdentityService;
        public DiscountController(IDiscountService discountService, ISharedIdentityService sharedIdentityService)
        {
            _discountService = discountService;
            _sharedIdentityService = sharedIdentityService;
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _discountService.GetAll();
            return CreateActionResultIntanse(result);
        }

        [HttpGet("getbyid")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _discountService.GetById(id);
            return CreateActionResultIntanse(result);
        }

        [HttpGet("getbycode")]
        public async Task<IActionResult> GetByCode(string code)
        {
            var userId = _sharedIdentityService.GetUserId;
            var result = await _discountService.GetByCodeAndUserId(code, userId);
            return CreateActionResultIntanse(result);
        }

        [HttpPost("save")]
        public async Task<IActionResult> Save(Models.Discount discount)
        {
            var result = await _discountService.Save(discount);
            return CreateActionResultIntanse(result);
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update(Models.Discount discount)
        {
            var result = await _discountService.Update(discount);
            return CreateActionResultIntanse(result);
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _discountService.Delete(id);
            return CreateActionResultIntanse(result);
        }

    }
}
